output "zone_id" {
  description = "ZoneID for the root domain for this environment"
  value       = cloudflare_zone.zone_definition.id
}
