variable "cloudflare_account_id" {
  type        = string
  description = "Cloudflare account ID."
  sensitive   = true
}

variable "cloudflare_zone_name" {
  type        = string
  description = "Cloudflare zone name."
}

variable "cloudflare_zone_plan" {
  type        = string
  description = "Cloudflare zone plan"
  default     = "free"
}

variable "cloudflare_zone_type" {
  type        = string
  description = "Cloudflare zone type"
  default     = "full"
}
